FROM "bitwardenrs/server:1.13.1-alpine" as bitwarden

FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

ENV ROCKET_ENV "staging"
ENV ROCKET_PORT=3000
ENV ROCKET_WORKERS=10
ENV DATA_FOLDER=/app/data
ENV CONFIG_FILE=/app/data/config.json

ENV SIGNUPS_ALLOWED=false
ENV INVITATIONS_ALLOWED=true
ENV WEBSOCKET_ENABLED=true

# Enable admin interface without requiring a token
# Interface is protected by the webserver (user has to be admin)
ENV DISABLE_ADMIN_TOKEN=true

RUN mkdir -p /app/data
VOLUME /app/data
EXPOSE 80
EXPOSE 3012

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN a2disconf other-vhosts-access-log
COPY apache.conf /etc/apache2/sites-enabled/bitwarden.conf
RUN a2enmod ldap authnz_ldap proxy proxy_http proxy_wstunnel rewrite

# Copies the files from the context (Rocket.toml file and web-vault)
# and the binary from the "build" stage to the current stage
COPY --from=bitwarden /web-vault /app/code/web-vault
COPY --from=bitwarden /bitwarden_rs /app/code/
COPY --from=bitwarden /Rocket.toml /app/code/

# configure supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

WORKDIR /app/code
COPY start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
