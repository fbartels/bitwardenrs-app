#!/bin/bash

set -eu

echo "=> Exporting env vars expected by Bitwarden"
export DOMAIN=$CLOUDRON_APP_ORIGIN
export SMTP_HOST=$CLOUDRON_MAIL_SMTP_SERVER
export SMTP_FROM=$CLOUDRON_MAIL_FROM
export SMTP_FROM_NAME=Bitwarden
export SMTP_PORT=$CLOUDRON_MAIL_SMTP_PORT
export SMTP_SSL=false
export SMTP_EXPLICIT_TLS=false
export SMTP_USERNAME=$CLOUDRON_MAIL_SMTP_USERNAME
export SMTP_PASSWORD=$CLOUDRON_MAIL_SMTP_PASSWORD
export SMTP_AUTH_MECHANISM=\"Plain\"
export SMTP_TIMEOUT=15

echo "=> Starting supervisord"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Bitwarden
